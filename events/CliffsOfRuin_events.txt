namespace = CliffsOfRuin_events

##Build
province_event = {
	id = CliffsOfRuin_events.1
	title = CliffsOfRuin_events.1.t
	desc = CliffsOfRuin_events.1.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1876 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1876 = { has_construction = canal }
				}
				1876 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.1.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_redrushes_climb
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1876
				}
				2095 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1876 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}

province_event = {
	id = CliffsOfRuin_events.2
	title = CliffsOfRuin_events.2.t
	desc = CliffsOfRuin_events.2.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1949 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1949 = { has_construction = canal }
				}
				1949 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.2.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_spoorland_lift
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1949
				}
				1810 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1949 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}

province_event = {
	id = CliffsOfRuin_events.3
	title = CliffsOfRuin_events.3.t
	desc = CliffsOfRuin_events.3.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1042 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1042 = { has_construction = canal }
				}
				1042 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.3.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_walkway_of_thorns
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1042
				}
				1882 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1042 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}

province_event = {
	id = CliffsOfRuin_events.4
	title = CliffsOfRuin_events.4.t
	desc = CliffsOfRuin_events.4.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1901 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1901 = { has_construction = canal }
				}
				1901 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.4.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_arca_noruin
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1901
				}
				1835 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1901 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}

province_event = {
	id = CliffsOfRuin_events.5
	title = CliffsOfRuin_events.5.t
	desc = CliffsOfRuin_events.5.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1031 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1031 = { has_construction = canal }
				}
				1031 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.5.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_arca_venaan
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1031
				}
				2757 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1031 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}

province_event = {
	id = CliffsOfRuin_events.6
	title = CliffsOfRuin_events.6.t
	desc = CliffsOfRuin_events.6.d
	picture = FORT_eventPicture
	goto = root
	
	major = yes
	is_triggered_only = yes
	
	trigger = {
		NOT = {
			has_province_modifier = ruin_cliff_passage_link
		}
		has_province_flag = f_building_ruin_cliff_passage
		had_province_flag = {
			flag = f_building_ruin_cliff_passage
			days = 1510
		}
		1128 = {
			NOT = { has_province_modifier = ruin_cliff_passage_link }
		}
	}
	
	immediate = {
		hidden_effect = {
			if = {	#finish it just in case
				limit = {
					1128 = { has_construction = canal }
				}
				1128 = { add_construction_progress = 1 }
			}
			owner = { set_country_flag = ruin_cliff_finished }
		}
	}
	
	option = {
        name = CliffsOfRuin_events.6.a
        ai_chance = { factor = 100 }
		goto = root
		
		if = {
			limit = {
				has_province_modifier = building_ruin_cliff_passage
			}
			remove_province_modifier = building_ruin_cliff_passage
		}
		clr_province_flag = f_building_ruin_cliff_passage
		add_permanent_province_modifier	= {
			name = m_arbeloch_ascensor
			duration = -1
			hidden = no
		}
		
		hidden_effect = {
			if = {
				limit = {
					province_id = 1128
				}
				1153 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
			else = {
				1128 = {		
					add_permanent_province_modifier	= {
						name = ruin_cliff_passage_link
						duration = -1
						hidden = no
					}
				}
			}
		}
		
		owner = {
			add_prestige = 5
			clr_country_flag = ruin_cliff_finished
		}
	}
}