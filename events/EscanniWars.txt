namespace = escanni_wars

country_event = {
	id = escanni_wars.1
	title = "escanni_wars.1.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.1.d"
	
	major = yes
	fire_only_once = yes
	
	trigger = {
		current_age = age_of_absolutism
		NOT = { has_global_flag = escanni_wars_bypass }
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		total_development = 150
		is_subject = no
		any_known_country = {
			OR = {
				culture_group = escanni
				primary_culture = marrodic
				primary_culture = iron_dwarf
				primary_culture = newfoot_halfling
			}
			capital_scope = {
				OR  = {
					region = south_castanor_region
					region = west_castanor_region
					region = inner_castanor_region
					area = cursewood_area
					area = whistlevale_area
				}
			}
			total_development = 150
			is_subject = no
		}
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = escanni_wars.1.a
		
		custom_tooltip = escanni_wars_tt
		set_global_flag = escanni_wars_global_flag
	}
}

# End of the Escanni Wars of Consolidation
country_event = {
	id = escanni_wars.2
	title = "escanni_wars.2.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.2.d"
	
	major = yes
	
	fire_only_once = yes
	
	trigger = {
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		had_global_flag = {
			flag = escanni_wars_global_flag
			days = 18250
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = escanni_wars.2.a
		
		custom_tooltip = escanni_wars_end_tt
		clr_global_flag = escanni_wars_global_flag
		
		if = {
			limit = {
				OR = {
					culture_group = escanni
					primary_culture = marrodic
					primary_culture = iron_dwarf
					primary_culture = newfoot_halfling
				}
				capital_scope = {
					OR  = {
						region = south_castanor_region
						region = west_castanor_region
						region = inner_castanor_region
						area = cursewood_area
						area = whistlevale_area
					}
				}
				is_subject = no
				num_of_owned_provinces_with = {
					value = 100
					
					OR = {
						region = south_castanor_region
						region = west_castanor_region
						region = inner_castanor_region
						area = cursewood_area
						area = whistlevale_area
					}
				}
				#Own Castonath
				owns = 831
				owns = 832
				owns = 833
			}
			country_event = { id = escanni_wars.10 days = 1 }
		}
		else = {
			random_country = {
				limit = {
					OR = {
						culture_group = escanni
						primary_culture = marrodic
						primary_culture = iron_dwarf
						primary_culture = newfoot_halfling
					}
					capital_scope = {
						OR  = {
							region = south_castanor_region
							region = west_castanor_region
							region = inner_castanor_region
							area = cursewood_area
							area = whistlevale_area
						}
					}
					is_subject = no
					num_of_owned_provinces_with = {
						value = 100
						
						OR = {
							region = south_castanor_region
							region = west_castanor_region
							region = inner_castanor_region
							area = cursewood_area
							area = whistlevale_area
						}
					}
					#Own Castonath
					owns = 831
					owns = 832
					owns = 833
				}
				country_event = { id = escanni_wars.10 days = 1 }
			}
		}
	}
}

#Escanni Wars Bypass
country_event = {
	id = escanni_wars.3
	title = "escanni_wars.3.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.3.d"
	
	major = yes
	fire_only_once = yes
	
	trigger = {
		current_age = age_of_absolutism
		NOT = { has_global_flag = escanni_wars_global_flag }
		is_year = 1640
		NOT = { is_year = 1650 }
		OR = {
			culture_group = escanni
			primary_culture = marrodic
			primary_culture = iron_dwarf
			primary_culture = newfoot_halfling
		}
		capital_scope = {
			OR  = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		is_subject = no
		num_of_owned_provinces_with = {
			value = 100
			
			OR = {
				region = south_castanor_region
				region = west_castanor_region
				region = inner_castanor_region
				area = cursewood_area
				area = whistlevale_area
			}
		}
		#Own Castonath
		owns = 831
		owns = 832
		owns = 833
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	option = {
		name = escanni_wars.3.a
		
		set_global_flag = escanni_wars_bypass
		country_event = { id = escanni_wars.10 days = 1 }
	}
}

# Escanni Wars winner reward
country_event = {
	id = escanni_wars.10
	title = "escanni_wars.10.t"
	picture = {	
		trigger = {
			has_dlc = "Emperor"
		}
		picture = ITALIAN_wars_eventPicture
	}
	picture = {	
		trigger = {
			NOT = { has_dlc = "Emperor" }
		}
		picture = MILITARY_CAMP_eventPicture
	}
	desc = "escanni_wars.10.d"
	
	major = yes
	is_triggered_only = yes
	
	option = { #Empire of Anbennar: 11-19
		name = escanni_wars.10.a
		trigger = {
			religion_group = cannorian
			government = monarchy
			hre_size = 1
		}
		
		custom_tooltip = usurp_emperorship_choice_tt
		set_country_flag = escanni_wars_usurp_emperorship
	}
	
	option = { #Castanor: 20-29
		name = escanni_wars.10.b
		trigger = {
			always = no
		}
		
	}
	
	option = { #Infernal Court: 30-39
		name = escanni_wars.10.c
		trigger = {
			always = no
		}
		
	}
	
	option = { #Black Demesne: 40-49
		name = escanni_wars.10.e
		trigger = {
			always = no
		}
		
	}
	
	option = { #Generic Conquest
		name = escanni_wars.10.y
		
		add_country_modifier = {
			name = escanni_wars_escanni_imperialism
			duration = -1
		}
	}
	
	option = { #Generic Peace
		name = escanni_wars.10.z
		
		add_country_modifier = {
			name = escanni_wars_escanni_peace
			duration = -1
		}
	}
}

# Empire of Anbennar: 11-19 #
# Emperorship Usurped
country_event = {
	id = escanni_wars.11
	title = "escanni_wars.11.t"
	picture = HRE_eventPicture
	desc = "escanni_wars.11.d"
	
	major = yes
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = { make_emperor = yes }
	}
	
	option = {
		name = escanni_wars.11.a
		
		tooltip = { make_emperor = yes }
		if = {
			limit = { NOT = { hre_reform_passed = emperor_erbkaisertum } }
			hre_inheritable = yes
			set_country_flag = usurp_emperorship_hereditary_flag
		}
		add_imperial_influence = 100
		add_country_modifier = {
			name = escanni_wars_escanni_emperor
			duration = -1
		}
	}
}

# Electors called into war
country_event = {
	id = escanni_wars.12
	title = "escanni_wars.12.t"
	picture = HRE_eventPicture
	desc = "escanni_war.12.d"
	
	is_triggered_only = yes
	
	option = {
		name = escanni_wars.12.a
		ai_chance = { factor = 100 }
		
		add_prestige = 25
		join_all_defensive_wars_of = emperor
	}
	
	option = {
		name = escanni_wars.12.b
		ai_chance = { factor = 0 }
		
		add_prestige = -25
		add_legitimacy = -25
	}
}

# Castanor: 20-29 #

# Infernal Court: 30-39 #

# Black Demesne: 40-49 #
